# confluence-parser

Java library to parse Confluence pages to JSON

To reference the library add the following lines to your `pom.xml`:

```
    <dependency>
      <groupId>me.bvn</groupId>
      <artifactId>page-parser</artifactId>
      <version>${page.parser.version}</version>
    </dependency>
```

Also add the following to your `settings.xml`:

```
    <repository>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <id>bintray-babinvn-maven</id>
      <name>bintray</name>
      <url>https://dl.bintray.com/babinvn/maven</url>
    </repository>
```

