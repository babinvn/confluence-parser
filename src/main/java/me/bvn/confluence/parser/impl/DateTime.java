package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.bvn.confluence.parser.model.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class DateTime implements TerminalNode {
    private static final String TYPE = "date";
    private final String datetime;

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        ObjectNode dateNode = mapper.createObjectNode();
        dateNode.put("_type", "date");
        dateNode.put("date", datetime);
        return dateNode;
    }
    @Override
    public String getText() {
        return datetime;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sDATE(%s)", StringUtils.repeat(' ', margin), datetime));
        }
    }

    public DateTime(String datetime) {
        this.datetime = datetime;
    }
}
