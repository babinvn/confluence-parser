package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.bvn.confluence.parser.model.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class PageLink implements TerminalNode {
    private static final String TYPE = "page";
    private final Long id;
    private final String title;

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        ObjectNode pageNode = mapper.createObjectNode();
        pageNode.put("_type", "page");
        pageNode.put("id", id);
        pageNode.put("title", title);
        return pageNode;
    }
    @Override
    public String getText() {
        return title;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sPAGE(%s)", StringUtils.repeat(' ', margin), title));
        }
    }

    public PageLink(Long id, String title) {
        this.id = id;
        this.title = title;
    }
}
