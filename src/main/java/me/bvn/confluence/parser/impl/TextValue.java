package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.model.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class TextValue implements TerminalNode {
    private static final String TYPE = "text";
    private String text;

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return text;
    }
    @Override
    public String getText() {
        return text;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sTEXT(%s)", StringUtils.repeat(' ', margin), text));
        }
    }

    public void concat(String text) {
        this.text = this.text.concat(text);
    }
    
    public TextValue(String text) {
        this.text = text;
    }
}
