package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.model.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class TaskStatus implements TerminalNode {
    private static final String TYPE = "boolean";
    private Boolean checked;

    public Boolean isChecked() {
        return checked;
    }
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return isChecked();
    }
    @Override
    public String getText() {
        return isChecked()? "\u2611": "\u2610";
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sTASKSTATUS(%s)", StringUtils.repeat(' ', margin), checked));
        }
    }
}
