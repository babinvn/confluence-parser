package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.model.TerminalNode;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class DecimalValue implements TerminalNode {
    private static final String TYPE = "number";
    private static final Pattern RE_NUMBER = Pattern.compile("^-?(?:\\d+(?:\\.\\d+)?|\\.\\d+)$");

    private final BigDecimal dec;
    private final String text;

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return dec;
    }
    @Override
    public String getText() {
        return text;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sDEC(%s)", StringUtils.repeat(' ', margin), dec.toPlainString()));
        }
    }

    public DecimalValue(String text) {
        this.text = text;
        this.dec = new BigDecimal(text);
    }

    public static boolean isDecimalValue(String text) {
        return RE_NUMBER.matcher(text).matches();
    }
}
