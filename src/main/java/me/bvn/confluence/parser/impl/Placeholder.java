package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.SubtreeNode;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class Placeholder implements SubtreeNode {
    // Placeholder is not taking any value
    @Override
    public void addNode(Node node) {
    }
    @Override
    public void addNodes(List<Node> nodes) {
    }
    @Override
    public List<Node> getNodes() {
        return Collections.emptyList();
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return null;
    }
    @Override
    public String getText() {
        return null;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sPLACEHOLDER", StringUtils.repeat(' ', margin)));
        }
    }
}
