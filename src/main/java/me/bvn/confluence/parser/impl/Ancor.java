package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.ValueList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class Ancor extends ValueList {
    private final String href;
    private TextValue text = null;

    @Override
    public void addNode(Node node) {
        if (node instanceof TextValue) {
            TextValue value = (TextValue)node;
            if (text == null) {
                text = value;
            } else {
                text.concat(value.getText());
            }
        }
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.forEach(node -> addNode(node));
    }
    @Override
    public List<Node> getNodes() {
        return Arrays.asList(text);
    }    
    @Override
    public Object getValue(ObjectMapper mapper) {
        ObjectNode ancor = mapper.createObjectNode();
        ancor.put("_type", "link");
        ancor.put("href", href);
        ancor.put("text", getText());
        return ancor;
    }
    @Override
    public String getText() {
        return text == null? null: text.getText();
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sA(%s)", StringUtils.repeat(' ', margin), href));
        }
    }
    @Override
    public List<Node> reduce(String type, Boolean csv) {
        if (type == null || "text".equals(type) || "link".equals(type)) {
            return Arrays.asList(this);
        } else {
            return null;
        }
    }

    public Ancor(String href) {
        this.href = href;
    }
}
