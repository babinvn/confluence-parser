package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.SubtreeNode;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class TaskBody implements SubtreeNode {
    private TextValue text = null;

    @Override
    public void addNode(Node node) {
        if (node instanceof TextValue) {
            TextValue value = (TextValue)node;
            if (text == null) {
                text = value;
            } else {
                text.concat(" ");
                text.concat(value.getText());
            }
        }
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.forEach(node -> addNode(node));
    }
    @Override
    public List<Node> getNodes() {
        return Arrays.asList(text);
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return null;
    }
    @Override
    public String getText() {
        return text == null? null: text.getText();
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sTASKBODY", StringUtils.repeat(' ', margin)));
        }
    }
}
