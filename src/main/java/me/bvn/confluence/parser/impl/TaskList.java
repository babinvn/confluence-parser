package me.bvn.confluence.parser.impl;

import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.ValueList;
import java.util.List;

public class TaskList extends ValueList {
    @Override
    public void addNode(Node node) {
        if (node instanceof Task) {
            nodes.add(node);
        }
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.forEach(node -> addNode(node));
    }
}
