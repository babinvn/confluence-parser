package me.bvn.confluence.parser.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.bvn.confluence.parser.model.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class User implements TerminalNode {
    private static final String TYPE = "user";
    private final String userKey;
    private final String fullName;
    private final String name;

    @Override
    public String getType() {
        return TYPE;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        ObjectNode userNode = mapper.createObjectNode();
        userNode.put("_type", "user"); //RI_USER.toString());
        userNode.put("key", userKey);
        userNode.put("fullName", fullName);
        userNode.put("name", name);
        return userNode;
    }
    @Override
    public String getText() {
        return fullName;
    }
    @Override
    public void dump(Logger logger, int margin) {
        if (logger != null) {
            logger.trace(String.format("%sUSER(%s)", StringUtils.repeat(' ', margin), fullName));
        }
    }

    public User(String userKey, String fullName, String name) {
        this.userKey = userKey;
        this.fullName = fullName;
        this.name = name;
    }
}
