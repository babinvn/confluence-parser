package me.bvn.confluence.parser.impl;

import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.ValueList;
import java.util.Arrays;
import java.util.List;

public class Task extends ValueList {
    // Convert "☑", "Task 1" to "☑ Task 1"
    @Override
    public List<Node> reduce(String type, Boolean csv) {
        List<Node> nodes = this.getNodes();
        if (
            "text".equals(type)
            && nodes.size() == 2
            && (nodes.get(0) instanceof TaskStatus)
            && (nodes.get(1) instanceof TaskBody)
        ) {
            TaskStatus taskStatus = (TaskStatus)nodes.get(0);
            TaskBody taskBody = (TaskBody)nodes.get(1);
            TextValue text = new TextValue(taskStatus.getText());
            taskBody.getNodes().forEach(node -> {
                String nodeText = node.getText();
                if (nodeText != null) {
                    text.concat(" ");
                    text.concat(nodeText);
                }
            });
            return Arrays.asList(text);
        }
        return super.reduce(type, csv);
    }
}
