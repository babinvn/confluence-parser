package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ValueList;
import me.bvn.confluence.parser.model.AttributeNode;
import me.bvn.confluence.parser.model.Node;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SchemaAttribute extends ValueList implements AttributeNode {
    private final String name;
    private final String type;
    private final Boolean csv;

    @Override
    public String getName() {
        return name;
    }
    @Override
    public String getType() {
        return type;
    }
    @Override
    public Boolean isCSV() {
        return csv;
    }
    public void populateDefinitions(com.fasterxml.jackson.databind.node.ObjectNode definitions, ObjectMapper mapper) {
        for (Node node : this.getNodes()) {
            if (node instanceof SchemaObject) {
                SchemaObject obj = (SchemaObject)node;
                obj.populateDefinitions(mapper);
            }
        }
    }

    public SchemaAttribute(String name, String type, Boolean csv){
        this.name = name;
        this.type = type;
        this.csv = csv;
    }
}
