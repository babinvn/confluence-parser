package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.PageResolver;
import me.bvn.confluence.parser.impl.PageLink;

public class DummyPageResolver implements PageResolver {
    @Override
    public PageLink getPageInfo(String spaceKey, String title) {
        return new PageLink(-1l, title);
    }    
}
