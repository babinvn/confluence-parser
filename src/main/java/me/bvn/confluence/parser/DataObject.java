package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ObjectNode;
import me.bvn.confluence.parser.model.AttributeNode;
import me.bvn.confluence.parser.model.Node;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class DataObject implements ObjectNode {
    private final String id;
    private final String type;
    private final List<Node> attributes = new ArrayList<>();

    @Override
    public String getId() {
        return id;
    }
    @Override
    public String getType() {
        return type;
    }
    @Override
    public void addNode(Node node) {
        if (node instanceof AttributeNode) {
            attributes.add(node);
        } else if (node instanceof ObjectNode) {
            ObjectNode other = (ObjectNode)node;
            addNodes(other.getNodes());
        }
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.forEach(node -> addNode(node));
    }
    @Override
    public List<Node> getNodes() {
        return attributes;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        return getJson(mapper);
    }
    @Override
    public String getText() {
        throw new UnsupportedOperationException();
    }
    @Override
    public void dump(Logger logger, int margin) {
        logger.trace(String.format("%s{", StringUtils.repeat(' ', margin)));
        attributes.forEach(attribute -> attribute.dump(logger, margin + 2));
        logger.trace(String.format("%s}", StringUtils.repeat(' ', margin)));
    }
    @Override
    public com.fasterxml.jackson.databind.node.ObjectNode getJson(ObjectMapper mapper) {
        com.fasterxml.jackson.databind.node.ObjectNode obj = mapper.createObjectNode();
        if (id != null) {
            obj.put("id", id);
        }
        if (type != null) {
            obj.put("_type", type);
        }
        attributes.forEach(node -> {
            assert(node instanceof AttributeNode);
            AttributeNode attribute = (AttributeNode)node;
            Object val = attribute.getValue(mapper);
            if (val == null) {
                obj.putNull(attribute.getName());
            } else if (val instanceof JsonNode) {
                obj.set(attribute.getName(), (JsonNode)val);
            } else if (val instanceof Boolean) {
                obj.put(attribute.getName(), (Boolean)val);
            } else if (val instanceof BigDecimal) {
                obj.put(attribute.getName(), (BigDecimal)val);
            } else if (val instanceof String) {
                obj.put(attribute.getName(), (String)val);
            } else {
                throw new RuntimeException(String.format("Unexpected attribute value type: %s", val.getClass()));
            }
        });
        return obj;
    }
    
    public DataObject(String id, String type) {
        this.id = id;
        this.type = type;
    }
}
