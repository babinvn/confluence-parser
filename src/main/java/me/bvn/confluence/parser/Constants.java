package me.bvn.confluence.parser;

import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import javax.xml.namespace.QName;

public interface Constants {
    static final QName HTML_TIME = new QName(XhtmlConstants.XHTML_NAMESPACE_URI, "time", "");
    static final QName HTML_DATETIME = new QName("datetime");
    static final QName HTML_A = new QName(XhtmlConstants.XHTML_NAMESPACE_URI, "a", "");
    static final QName HTML_P = new QName(XhtmlConstants.XHTML_NAMESPACE_URI, "p", "");
    static final QName HTML_HREF = new QName("href");
    static final QName HTML_CLASS = new QName("class");

    static final QName RI_USER = new QName(XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI, "user", XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    static final QName RI_USER_KEY = new QName(XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI, "userkey", XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    static final QName AC_PLACEHOLDER = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "placeholder", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);
    static final QName RI_PAGE = new QName(XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI, "page", XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    static final QName RI_CONTENT_TITLE = new QName(XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI, "content-title", XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    static final QName RI_SPACE_KEY = new QName(XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI, "space-key", XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    static final QName AC_TASK_LIST = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "task-list", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);
    static final QName AC_TASK = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "task", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);
    static final QName AC_TASK_ID = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "task-id", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);
    static final QName AC_TASK_STATUS = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "task-status", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);
    static final QName AC_TASK_BODY = new QName(XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_URI, "task-body", XhtmlConstants.CONFLUENCE_XHTML_NAMESPACE_ALTERNATE_PREFIX);

    static final String OBJECT_CLASS_NAME           = "dsobject";
    static final String OBJECTID_CLASS_PREFIX       = "dsobjid-";
    static final String OBJECT_TYPE_CLASS_PREFIX    = "dsobjtype-";
    static final String ATTRIBUTE_NAME_CLASS_PREFIX = "dsattr-";
    static final String ATTRIBUTE_TYPE_CLASS_PREFIX = "dsattrtype-";
    static final String VALIDATOR_NUMBER            = "validator-number";
    static final String VALIDATOR_CSV               = "validator-csv";
    static final String VALIDATOR_BOOLEAN           = "validator-boolean";
}
