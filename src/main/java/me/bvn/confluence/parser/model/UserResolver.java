package me.bvn.confluence.parser.model;

import me.bvn.confluence.parser.impl.User;

public interface UserResolver {
    User getUserInfo(String userKey);
}
