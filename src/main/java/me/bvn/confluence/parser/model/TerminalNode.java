package me.bvn.confluence.parser.model;

public interface TerminalNode extends Node {
    String getType();
}
