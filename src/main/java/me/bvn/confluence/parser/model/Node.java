package me.bvn.confluence.parser.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;

public interface Node {
    Object getValue(ObjectMapper mapper);
    String getText();
    void dump(Logger logger, int margin);
}
