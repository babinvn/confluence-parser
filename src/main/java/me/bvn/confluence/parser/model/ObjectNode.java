package me.bvn.confluence.parser.model;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface ObjectNode extends SubtreeNode {
    String getId();
    String getType();
    com.fasterxml.jackson.databind.node.ObjectNode getJson(ObjectMapper mapper);
}
