package me.bvn.confluence.parser.model;

import java.util.List;

public interface SubtreeNode extends Node {
    void addNode(Node node);
    void addNodes(List<Node> nodes);
    List<Node> getNodes();
}
