package me.bvn.confluence.parser.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.impl.DecimalValue;
import me.bvn.confluence.parser.impl.TextValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class ValueList implements SubtreeNode {
    protected final List<Node> nodes = new ArrayList<>();

    @Override
    public void addNode(Node node) {
        nodes.add(node);
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.addAll(nodes);
    }
    @Override
    public List<Node> getNodes() {
        return nodes;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        throw new UnsupportedOperationException();
    }
    @Override
    public String getText() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public void dump(Logger logger, int margin) {
        logger.trace(String.format("%s{", StringUtils.repeat(' ', margin)));
        nodes.forEach(node -> node.dump(logger, margin + 2));
        logger.trace(String.format("%s}", StringUtils.repeat(' ', margin)));
    }

    public List<Node> reduce(String type, Boolean csv) {
        List<Node> result = merge(this.getNodes());
        if (csv) {
            result = split(result, type);
        }
        return reduce(result, type , csv);
    }
    // Merge adjacent text nodes
    protected static List<Node> merge(List<Node> nodes) {
        List<Node> merged = new ArrayList<>();
        for (Node node : nodes) {
            if (merged.isEmpty()) {
                merged.add(node);
            } else if (node instanceof TextValue && merged.get(merged.size() - 1) instanceof TextValue) {
                TextValue source = (TextValue)node;
                TextValue target = (TextValue)merged.get(merged.size() - 1);
                target.concat(source.getText());
            } else {
                merged.add(node);
            }
        }
        return merged;
    }
    // If CSV split strings with commas
    protected static List<Node> split(List<Node> nodes, String type) {
        List<Node> splitted = new ArrayList<>();
        for (Node node : nodes) {
            if (node instanceof ObjectNode) {
                splitted.add(node);
            } else if (node instanceof TextValue) {
                String text = ((TextValue) node).getText();
                if (text == null) {
                    continue;
                }
                Arrays.asList(text.split(",")).forEach(subtext -> {
                    if (type == null || "text".equals(type)) {
                        splitted.add(new TextValue(subtext.trim()));
                    } else if ("number".equals(type)) {
                        if (DecimalValue.isDecimalValue(subtext.trim())) {
                            splitted.add(new DecimalValue(subtext.trim()));
                        }
                    }
                });
            } else {
                splitted.add(node);
            }
        }
        return splitted;
    }
    // Reduce node list to desired type
    protected static List<Node> reduce(List<Node> nodes, String type, Boolean csv) {
        List<Node> reduced = new ArrayList<>();
        for (Node node : nodes) {
            if (node instanceof ObjectNode) {
                ObjectNode objectNode = (ObjectNode)node;
                if (type == null || type.equals(objectNode.getType())) {
                    reduced.add(node);
                }
            } else if (node instanceof ValueList) {
                ValueList valueList = (ValueList)node;
                List<Node> rr = valueList.reduce(type, csv);
                if (rr != null)
                    reduced.addAll(rr);
            } else if (node instanceof TerminalNode) {
                TerminalNode terminalNode = (TerminalNode)node;
                if (type == null || "text".equals(type) || type.equals(terminalNode.getType())) {
                    reduced.add(node);
                } else if ("number".equals(type) && terminalNode instanceof TextValue) {
                    TextValue textValue = (TextValue)terminalNode;
                    if (DecimalValue.isDecimalValue(textValue.getText())) {
                        reduced.add(new DecimalValue(textValue.getText()));
                    }
                }
            } else if (node instanceof SubtreeNode) {
                if (type == null || "text".equals(type)) {
                    reduced.add(node);
                }
            }
        }
        return reduced;
    }
}
