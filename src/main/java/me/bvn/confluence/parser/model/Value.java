package me.bvn.confluence.parser.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import me.bvn.confluence.parser.impl.TextValue;

public abstract class Value {
    public abstract ArrayNode write(ObjectMapper mapper, ArrayNode array);
    public abstract TextValue getValueAsText();
}
