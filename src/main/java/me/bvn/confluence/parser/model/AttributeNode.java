package me.bvn.confluence.parser.model;

public interface AttributeNode extends SubtreeNode {
    String getName();
    String getType();
    Boolean isCSV();
}
