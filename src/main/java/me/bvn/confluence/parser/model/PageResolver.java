package me.bvn.confluence.parser.model;

import me.bvn.confluence.parser.impl.PageLink;

public interface PageResolver {
    PageLink getPageInfo(String spaceKey, String title);
}
