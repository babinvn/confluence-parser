package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ObjectNode;
import me.bvn.confluence.parser.model.Node;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class SchemaObject implements ObjectNode {
    private final String type;
    private final List<Node> attributes = new ArrayList<>();
    private SchemaObject root;
    private com.fasterxml.jackson.databind.node.ObjectNode definitions;

    @Override
    public String getType() {
        return type;
    }
    @Override
    public String getId() {
        throw new UnsupportedOperationException();
    }
    @Override
    public void addNode(Node node) {
        if (node instanceof SchemaAttribute) {
            attributes.add((SchemaAttribute)node);
        } else if (node instanceof ObjectNode) {
            ObjectNode other = (ObjectNode)node;
            addNodes(other.getNodes());
        }
    }
    @Override
    public void addNodes(List<Node> nodes) {
        nodes.forEach(node -> addNode(node));
    }
    @Override
    public List<Node> getNodes() {
        return attributes;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void dump(Logger logger, int margin) {
        logger.trace(String.format("%s{", StringUtils.repeat(' ', margin)));
        attributes.forEach(attribute -> attribute.dump(logger, margin + 2));
        logger.trace(String.format("%s}", StringUtils.repeat(' ', margin)));
    }
    @Override
    public String getText() {
        throw new UnsupportedOperationException();
    }
    @Override
    public com.fasterxml.jackson.databind.node.ObjectNode getJson(ObjectMapper mapper) {
        try (InputStream in = this.getClass().getResourceAsStream("/schema.base.json")) {
            com.fasterxml.jackson.databind.node.ObjectNode root = (com.fasterxml.jackson.databind.node.ObjectNode)mapper.readTree(in);
            com.fasterxml.jackson.databind.node.ObjectNode properties = (com.fasterxml.jackson.databind.node.ObjectNode)root.get("properties");
            if (this.root.definitions == null)
                this.root.definitions = (com.fasterxml.jackson.databind.node.ObjectNode)root.get("definitions");

            this.attributes.forEach(node -> {
                assert(node instanceof SchemaAttribute);
                SchemaAttribute attribute = (SchemaAttribute)node;
                com.fasterxml.jackson.databind.node.ObjectNode property = mapper.createObjectNode();
                property.put("$ref", getAttributeTypeDefinition(attribute.getType()));
                properties.set(attribute.getName(), property);
            });

            ArrayNode requred = (ArrayNode)root.get("required");
            this.attributes.forEach(node -> {
                assert(node instanceof SchemaAttribute);
                SchemaAttribute attribute = (SchemaAttribute)node;
                requred.add(attribute.getName());
            });

            populateDefinitions(mapper);
            return root;
        } catch (IOException ex) {
            throw new ParserException("Failed to load base schema object", ex);
        }
    }

    private static String getAttributeTypeDefinition(String type) {
        if (type == null) {
            return "#/definitions/vbp-any";
        } else {
            switch (type) {
                case "text":
                    return "#/definitions/vbp-text";
                case "boolean":
                    return "#/definitions/vbp-boolean";
                case "date":
                    return "#/definitions/vbp-date";
                case "link":
                    return "#/definitions/vbp-link";
                case "number":
                    return "#/definitions/vbp-number";
                case "page":
                    return "#/definitions/vbp-page";
                case "user":
                    return "#/definitions/vbp-user";
                default:
                    return "#/definitions/" + type;
            }
        }
    }
    private com.fasterxml.jackson.databind.node.ObjectNode createTypeRecDefinition(ObjectMapper mapper) {
        /*
        "project-rec": {
            "type": "object",
            "properties": {
                "_type": { "enum": ["project"] },
                "cost": { "$ref": "#/definitions/vbp-number" },
                "manager": { "$ref": "#/definitions/vbp-user" },
                "start": { "$ref": "#/definitions/vbp-date" },
                "comment": { "type": "string" }
            }
        }
        */
        com.fasterxml.jackson.databind.node.ObjectNode typeRec = mapper.createObjectNode();
        typeRec.put("type", "object");
        com.fasterxml.jackson.databind.node.ObjectNode properties = mapper.createObjectNode();
        com.fasterxml.jackson.databind.node.ObjectNode _type = mapper.createObjectNode();
        com.fasterxml.jackson.databind.node.ArrayNode typeEnum = mapper.createArrayNode();
        typeEnum.add(this.type);
        _type.set("enum", typeEnum);
        properties.set("_type", _type);
        attributes.forEach(node -> {
            assert(node instanceof SchemaAttribute);
            SchemaAttribute attribute = (SchemaAttribute)node;
            com.fasterxml.jackson.databind.node.ObjectNode attr = mapper.createObjectNode();
            attr.put("$ref", getAttributeTypeDefinition(attribute.getType()));
            properties.set(attribute.getName(), attr);
        });
        typeRec.set("properties", properties);
        return typeRec;
    }
    private com.fasterxml.jackson.databind.node.ObjectNode createTypeDefinition(ObjectMapper mapper) {
        /*
        "project": {
            "anyOf": [
                { "$ref": "#/definitions/project-rec" },
                { "type": "array", "items": { "$ref": "#/definitions/project-rec" } },
                { "type": "null" }
            ]
        }
        */
        com.fasterxml.jackson.databind.node.ObjectNode type = mapper.createObjectNode();
        com.fasterxml.jackson.databind.node.ArrayNode anyOf = mapper.createArrayNode();
        com.fasterxml.jackson.databind.node.ObjectNode refType = mapper.createObjectNode();
        refType.put("$ref", String.format("#/definitions/%s-rec", this.type));
        anyOf.add(refType);
        com.fasterxml.jackson.databind.node.ObjectNode arrayType = mapper.createObjectNode();
        arrayType.put("type", "array");
        arrayType.set("items", refType.deepCopy());
        anyOf.add(arrayType);
        com.fasterxml.jackson.databind.node.ObjectNode nullType = mapper.createObjectNode();
        nullType.put("type", "null");
        anyOf.add(nullType);
        type.set("anyOf", anyOf);
        return type;
    }
    public void populateDefinitions(ObjectMapper mapper) {
        if (this.root.definitions.has(type)) {
            return; // already defined
        }
        if (type != null) {
            this.root.definitions.set(String.format("%s-rec", type), createTypeRecDefinition(mapper));
            this.root.definitions.set(type, createTypeDefinition(mapper));
        }
        this.attributes.forEach(node -> {
            assert(node instanceof SchemaAttribute);
            SchemaAttribute attribute = (SchemaAttribute)node;
            attribute.populateDefinitions(this.root.definitions, mapper);
        });
    }

    public SchemaObject() {
        this(null);
    }
    public SchemaObject(String type) {
        this(type, null);
    }
    public SchemaObject(String type, SchemaObject root) {
        this.type = type;
        this.root = root == null? this: root;
    }
}
