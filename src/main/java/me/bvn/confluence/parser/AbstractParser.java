package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ObjectNode;
import me.bvn.confluence.parser.model.AttributeNode;
import me.bvn.confluence.parser.model.PageResolver;
import me.bvn.confluence.parser.model.UserResolver;
import me.bvn.confluence.parser.model.SubtreeNode;
import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.impl.TextValue;
import me.bvn.confluence.parser.impl.TaskStatus;
import java.util.Stack;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

public abstract class AbstractParser {    
    protected abstract AttributeNode createAttribute(HtmlClassParser classParser);
    protected abstract ObjectNode createObject(HtmlClassParser classParser);
    protected abstract void startRiUser(StartElement elt);
    protected abstract void endRiUser(EndElement elt);
    protected abstract void startRiPage(StartElement elt);
    protected abstract void endRiPage(EndElement elt);
    protected abstract void startTime(StartElement elt);
    protected abstract void endTime(EndElement elt);
    protected abstract void startPlaceholder(StartElement elt);
    protected abstract void endPlaceholder(EndElement elt);
    protected abstract void startA(StartElement elt);
    protected abstract void endA(EndElement elt);
    protected abstract void startP(StartElement elt);
    protected abstract void endP(EndElement elt);
    protected abstract void startAcTaskList(StartElement elt);
    protected abstract void endAcTaskList(EndElement elt);
    protected abstract void startAcTask(StartElement elt);
    protected abstract void endAcTask(EndElement elt);
    protected abstract void startAcTaskId(StartElement elt);
    protected abstract void endAcTaskId(EndElement elt);
    protected abstract void startAcTaskBody(StartElement elt);
    protected abstract void endAcTaskBody(EndElement elt);
    protected abstract void startAcTaskStatus(StartElement elt);
    protected abstract void endAcTaskStatus(EndElement elt);
    
    protected final Logger logger;
    protected final XMLEventReader reader;
    protected final UserResolver userResolver;
    protected final PageResolver pageResolver;
    protected final Stack<Node> data = new Stack();
    protected final Stack<Integer> tree = new Stack();
    protected String attributeType;
    
    protected void startElement(StartElement elt) {
        if (logger != null) {
            logger.trace(String.format("%s<%s:%s>", StringUtils.repeat(" ", 2*tree.size()), elt.getName().getPrefix(), elt.getName().getLocalPart()));
        }
        
        if (Constants.RI_USER.equals(elt.getName())){
            startRiUser(elt);
        } else if (Constants.RI_PAGE.equals(elt.getName())){
            startRiPage(elt);
        } else if (Constants.HTML_TIME.equals(elt.getName())) {
            startTime(elt);
        } else if (Constants.AC_PLACEHOLDER.equals(elt.getName())) {
            startPlaceholder(elt);
        } else if (Constants.HTML_A.equals(elt.getName())) {
            startA(elt);
        } else if (Constants.HTML_P.equals(elt.getName())) {
            startP(elt);
        } else if (Constants.AC_TASK_LIST.equals(elt.getName())){
            startAcTaskList(elt);
        } else if (Constants.AC_TASK.equals(elt.getName())){
            startAcTask(elt);
        } else if (Constants.AC_TASK_ID.equals(elt.getName())){
            startAcTaskId(elt);
        } else if (Constants.AC_TASK_BODY.equals(elt.getName())){
            startAcTaskBody(elt);
        } else if (Constants.AC_TASK_STATUS.equals(elt.getName())){
            startAcTaskStatus(elt);
        } else {
            HtmlClassParser classParser = new HtmlClassParser(elt);
            int countDataNodes = 0;
            if (classParser.getDataAttributeName() != null) {
                // Element is an attribute
                AttributeNode attribute = createAttribute(classParser);
                attributeType = attribute.getType();
                data.push(attribute);
                countDataNodes++;
            }

            if (classParser.isDataObject() || classParser.getDataObjectId() != null){
                // Element is an object
                ObjectNode obj = createObject(classParser);
                data.push(obj);
                countDataNodes++;
            }
            tree.push(countDataNodes);
        }
    }
    protected void endElement(EndElement elt) {
        if (Constants.RI_USER.equals(elt.getName())){
            endRiUser(elt);
        } else if (Constants.RI_PAGE.equals(elt.getName())){
            endRiPage(elt);
        } else if (Constants.HTML_TIME.equals(elt.getName())) {
            endTime(elt);
        } else if (Constants.AC_PLACEHOLDER.equals(elt.getName())) {
            endPlaceholder(elt);
        } else if (Constants.HTML_A.equals(elt.getName())) {
            endA(elt);
        } else if (Constants.HTML_P.equals(elt.getName())) {
            endP(elt);
        } else if (Constants.AC_TASK_LIST.equals(elt.getName())){
            endAcTaskList(elt);
        } else if (Constants.AC_TASK.equals(elt.getName())){
            endAcTask(elt);
        } else if (Constants.AC_TASK_ID.equals(elt.getName())){
            endAcTaskId(elt);
        } else if (Constants.AC_TASK_BODY.equals(elt.getName())){
            endAcTaskBody(elt);
        } else if (Constants.AC_TASK_STATUS.equals(elt.getName())){
            endAcTaskStatus(elt);
        } else {
            int countDataNodes = tree.pop();
            while (countDataNodes > 0) {
                Node node = data.pop();
                Node parent = data.peek();
                if (parent instanceof SubtreeNode) {
                    SubtreeNode subtreeNode = (SubtreeNode)parent;
                    subtreeNode.addNode(node);
                } else {
                    throw new ParserException(String.format("Cannot add node %s to terminal node %s", node.getClass(), parent.getClass()));
                }
                countDataNodes--;
            }
        }
        if (logger != null) {
            logger.trace(String.format("%s</%s:%s>", StringUtils.repeat(" ", 2*tree.size()), elt.getName().getPrefix(), elt.getName().getLocalPart()));
        }
    }
    protected void characters (Characters ch){
        String text = ch.getData();
        if (text == null) {
            return;
        }
        text = text.replace("\u00A0", " ").trim();
        if (text.isEmpty()) {
            return;
        }
        if (logger != null) {
            logger.trace(String.format("%s\"%s\"", StringUtils.repeat(" ", 2*tree.size()), text));
        }

        if (data.peek() instanceof TaskStatus) {
            TaskStatus checkboxStatus = (TaskStatus)data.peek();
            if ("incomplete".equals(text)) {
                checkboxStatus.setChecked(Boolean.FALSE);
            } else if ("complete".equals(text)) {
                checkboxStatus.setChecked(Boolean.TRUE);
            }
        } else if (data.peek() instanceof SubtreeNode) {
            SubtreeNode node = (SubtreeNode)data.peek();
            node.addNode(new TextValue(text));
        }
    }

    protected void parse() throws XMLStreamException {
        while (reader.hasNext()){
            XMLEvent evt = reader.nextEvent();
            if (evt.isStartElement()){
                startElement(evt.asStartElement());
            } else if (evt.isEndElement()) {
                endElement(evt.asEndElement());
            } else if (evt.isCharacters()) {
                characters(evt.asCharacters());
            }
        }
        assert(data.size() == 1);
        assert(tree.isEmpty());
        data.get(0).dump(logger, 0);
    }
    protected String lookupAttributeType() {
        for (int i = data.size() - 1; i >= 0; i++) {
            if (data.get(i) instanceof AttributeNode) {
                AttributeNode attr = (AttributeNode)data.get(i);
                return attr.getType();
            }
        }
        return null;
    }

    public AbstractParser(XMLEventReader reader, UserResolver userResolver, PageResolver pageResolver, Logger logger) {
        this.reader = reader;
        this.userResolver = userResolver;
        this.pageResolver = pageResolver;
        this.logger = logger;
    }
}
