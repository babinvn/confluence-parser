package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ValueList;
import me.bvn.confluence.parser.model.AttributeNode;
import me.bvn.confluence.parser.model.Node;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class DataAttribute extends ValueList implements AttributeNode {
    private final String name;
    private final String type;
    private final Boolean csv;
    public String filter;

    @Override
    public String getName() {
        return name;
    }
    @Override
    public String getType() {
        return type;
    }
    @Override
    public Boolean isCSV() {
        return csv;
    }
    @Override
    public Object getValue(ObjectMapper mapper) {
        List<Node> values = reduce(this.type, this.csv);
        if (values.isEmpty()) {
            return null;
        } else if (values.size() == 1) {
            return "text".equals(this.type)
                    ? values.get(0).getText()
                    : values.get(0).getValue(mapper);
        } else {
            ArrayNode arr = mapper.createArrayNode();
            values.forEach(value -> {
                Object val = "text".equals(this.type)
                        ? value.getText()
                        : value.getValue(mapper);
                if (val == null) {
                    // arr.addNull();
                } else if (val instanceof JsonNode) {
                    arr.add((JsonNode)val);
                } else if (val instanceof Boolean) {
                    arr.add((Boolean)val);
                } else if (val instanceof BigDecimal) {
                    arr.add((BigDecimal)val);
                } else if (val instanceof String) {
                    arr.add((String)val);
                } else {
                    throw new RuntimeException(String.format("Unexpected attribute value type: %s", val.getClass()));
                }
            });
            return arr;
        }
    }
    @Override
    public void dump(Logger logger, int margin) {
        logger.trace(String.format("%s%s={", StringUtils.repeat(' ', margin), name));
        nodes.forEach(node -> node.dump(logger, margin + 2));
        logger.trace(String.format("%s}", StringUtils.repeat(' ', margin)));        
    }

    public DataAttribute(String name, String type, Boolean csv){
        this.name = name;
        this.type = type;
        this.csv = csv;
    }
}
