package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ObjectNode;
import me.bvn.confluence.parser.model.AttributeNode;
import me.bvn.confluence.parser.model.PageResolver;
import me.bvn.confluence.parser.model.UserResolver;
import me.bvn.confluence.parser.model.Node;
import me.bvn.confluence.parser.model.SubtreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.bvn.confluence.parser.impl.Ancor;
import me.bvn.confluence.parser.impl.DateTime;
import me.bvn.confluence.parser.impl.PageLink;
import me.bvn.confluence.parser.impl.Paragraph;
import me.bvn.confluence.parser.impl.Placeholder;
import me.bvn.confluence.parser.impl.Task;
import me.bvn.confluence.parser.impl.TaskBody;
import me.bvn.confluence.parser.impl.TaskList;
import me.bvn.confluence.parser.impl.TaskStatus;
import me.bvn.confluence.parser.impl.User;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import org.slf4j.Logger;

public class DataParser extends AbstractParser {
    @Override
    protected AttributeNode createAttribute(HtmlClassParser classParser) {
        return new DataAttribute(
            classParser.getDataAttributeName(),
            classParser.getDataAttributeType(),
            classParser.isCSV()
        );
    }
    @Override
    protected ObjectNode createObject(HtmlClassParser classParser) {
        return new DataObject(classParser.getDataObjectId(), classParser.getDataObjectType());
    }

    @Override
    protected void startRiUser(StartElement elt) {
        String userKey = elt.getAttributeByName(Constants.RI_USER_KEY).getValue();
        User user = userResolver.getUserInfo(userKey);
        if (user != null) {
            assert(data.peek() instanceof SubtreeNode);
            SubtreeNode parent = (SubtreeNode)data.peek();
            parent.addNode(user);
        }
        tree.push(0);
    }
    @Override
    protected void endRiUser(EndElement elt) {
        int c = tree.pop();
        assert(c == 0);
    }
    @Override
    protected void startRiPage(StartElement elt) {
        String title = elt.getAttributeByName(Constants.RI_CONTENT_TITLE).getValue();
        String spaceKey = elt.getAttributeByName(Constants.RI_SPACE_KEY) == null? null
                : elt.getAttributeByName(Constants.RI_SPACE_KEY).getValue();
        PageLink page = pageResolver.getPageInfo(spaceKey, title);
        if (page != null){
            assert(data.peek() instanceof SubtreeNode);
            SubtreeNode parent = (SubtreeNode)data.peek();
            parent.addNode(page);
        }
        tree.push(0);
    }
    @Override
    protected void endRiPage(EndElement elt) {
        int c = tree.pop();
        assert(c == 0);
    }
    @Override
    protected void startTime(StartElement elt) {
        String datetime = elt.getAttributeByName(Constants.HTML_DATETIME).getValue();
        if (datetime != null){
            DateTime time = new DateTime(datetime.trim());
            assert(data.peek() instanceof SubtreeNode);
            SubtreeNode parent = (SubtreeNode)data.peek();
            parent.addNode(time);
        }
        tree.push(0);
    }
    @Override
    protected void endTime(EndElement elt) {
        int c = tree.pop();
        assert(c == 0);
    }
    @Override
    protected void startPlaceholder(StartElement elt) {
        start(Placeholder.class);
    }
    @Override
    protected void endPlaceholder(EndElement elt) {
        data.pop(); // placeholder is always discarded
        int c = tree.pop();
        assert(c == 1);
    }
    @Override
    protected void startA(StartElement elt) {
        String href = elt.getAttributeByName(Constants.HTML_HREF) == null? null
                : elt.getAttributeByName(Constants.HTML_HREF).getValue();
        data.push(new Ancor(href));
        tree.push(1);
    }
    @Override
    protected void endA(EndElement elt) {
        end(Ancor.class);
    }
    @Override
    protected void startP(StartElement elt) {
        start(Paragraph.class);
    }
    @Override
    protected void endP(EndElement elt) {
        end(Paragraph.class);
    }
    @Override
    protected void startAcTaskList(StartElement elt) {
        start(TaskList.class);
    }
    @Override
    protected void endAcTaskList(EndElement elt) {
        end(TaskList.class);
    }
    @Override
    protected void startAcTask(StartElement elt) {
        start(Task.class);
    }
    @Override
    protected void endAcTask(EndElement elt) {
        end(Task.class);
    }
    @Override
    protected void startAcTaskId(StartElement elt) {
        startPlaceholder(elt);
    }
    @Override
    protected void endAcTaskId(EndElement elt) {
        endPlaceholder(elt);
    }
    @Override
    protected void startAcTaskBody(StartElement elt) {
        start(TaskBody.class);
    }
    @Override
    protected void endAcTaskBody(EndElement elt) {
        end(TaskBody.class);
    }
    @Override
    protected void startAcTaskStatus(StartElement elt) {
        start(TaskStatus.class);
    }
    @Override
    protected void endAcTaskStatus(EndElement elt) {
        end(TaskStatus.class);
    }
    protected <T extends Node> void start(Class<T> c) {
        try {
            data.push(c.newInstance());
            tree.push(1);
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }
    protected <T extends Node> void end(Class<T> clazz) {
        int c = tree.pop();
        assert(c == 1);
        Node node = data.pop();
        assert(node.getClass().equals(clazz));
        T obj = (T)node;
        assert(data.peek() instanceof SubtreeNode);
        SubtreeNode parent = (SubtreeNode)data.peek();
        parent.addNode(obj);
    }

    protected DataParser(XMLEventReader reader, UserResolver userResolver, PageResolver pageResolver, Logger logger) {
        super(reader, userResolver, pageResolver, logger);
    }

    public static com.fasterxml.jackson.databind.node.ObjectNode parse(
        XMLEventReader reader, UserResolver userResolver, PageResolver pageResolver, Logger logger
    ) throws XMLStreamException {
        DataParser parser = new DataParser(reader, userResolver, pageResolver, logger);
        DataObject root = new DataObject(null, null);
        parser.data.push(root);
        parser.parse();
        //root.dump(0);
        ObjectMapper mapper = new ObjectMapper();
        return root.getJson(mapper);
    }
}
