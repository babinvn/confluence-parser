package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.ObjectNode;
import me.bvn.confluence.parser.model.AttributeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import org.slf4j.Logger;

public class SchemaParser extends AbstractParser {
    private final SchemaObject root;
    
    @Override
    protected AttributeNode createAttribute(HtmlClassParser classParser) {
        return new SchemaAttribute(
            classParser.getDataAttributeName(),
            classParser.getDataAttributeType(),
            classParser.isCSV()
        );
    }
    @Override
    protected ObjectNode createObject(HtmlClassParser classParser) {
        return new SchemaObject(classParser.getDataObjectType(), root);
    }

    @Override
    protected void startRiUser(StartElement elt) {
    }
    @Override
    protected void endRiUser(EndElement elt) {
    }
    @Override
    protected void startRiPage(StartElement elt) {
    }
    @Override
    protected void endRiPage(EndElement elt) {
    }
    @Override
    protected void startTime(StartElement elt) {
    }
    @Override
    protected void endTime(EndElement elt) {
    }
    @Override
    protected void startPlaceholder(StartElement elt) {
    }
    @Override
    protected void endPlaceholder(EndElement elt) {
    }
    @Override
    protected void startA(StartElement elt) {
    }
    @Override
    protected void endA(EndElement elt) {
    }
    @Override
    protected void startP(StartElement elt) {
    }
    @Override
    protected void endP(EndElement elt) {
    }
    @Override
    protected void startAcTaskList(StartElement elt) {
    }
    @Override
    protected void endAcTaskList(EndElement elt) {
    }
    @Override
    protected void startAcTask(StartElement elt) {
    }
    @Override
    protected void endAcTask(EndElement elt) {
    }
    @Override
    protected void startAcTaskId(StartElement elt) {
    }
    @Override
    protected void endAcTaskId(EndElement elt) {
    }
    @Override
    protected void startAcTaskBody(StartElement elt) {
    }
    @Override
    protected void endAcTaskBody(EndElement elt) {
    }
    @Override
    protected void startAcTaskStatus(StartElement elt) {
    }
    @Override
    protected void endAcTaskStatus(EndElement elt) {
    }

    protected SchemaParser(XMLEventReader reader, Logger logger, SchemaObject root) {
        super(reader, new DummyUserResolver(), new DummyPageResolver(), logger);
        this.root = root;
    }

    public static com.fasterxml.jackson.databind.node.ObjectNode parse(XMLEventReader reader, Logger logger) throws XMLStreamException {
        SchemaObject root = new SchemaObject();
        SchemaParser parser = new SchemaParser(reader, logger, root);
        parser.data.push(root);
        parser.parse();
        // root.dump(logger, 0);
        ObjectMapper mapper = new ObjectMapper();
        return root.getJson(mapper);
    }
}
