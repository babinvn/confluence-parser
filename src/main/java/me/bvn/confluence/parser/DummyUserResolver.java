package me.bvn.confluence.parser;

import me.bvn.confluence.parser.model.UserResolver;
import me.bvn.confluence.parser.impl.User;

public class DummyUserResolver implements UserResolver {
    @Override
    public User getUserInfo(String userKey) {
        if (userKey == null) {
            return null;
        }
        return new User(userKey, userKey, userKey);
    }    
}
