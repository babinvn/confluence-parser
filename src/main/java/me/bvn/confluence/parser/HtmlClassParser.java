package me.bvn.confluence.parser;

import java.util.Arrays;
import javax.xml.stream.events.StartElement;

public class HtmlClassParser {
    private boolean dataObject = false;
    private boolean number = false;
    private boolean csv = false;
    private String dataObjectId = null;
    private String dataObjectType = null;
    private String dataAttributeName = null;
    private String dataAttributeType = null;

    public boolean isDataObject() {
        return dataObject;
    }
    public boolean isNumber() {
        return number;
    }
    public boolean isCSV() {
        return csv;
    }
    public String getDataObjectId() {
        return dataObjectId;
    }
    public String getDataObjectType() {
        return dataObjectType;
    }
    public String getDataAttributeName() {
        return dataAttributeName;
    }
    public String getDataAttributeType() {
        if (dataAttributeType != null) {
            return dataAttributeType;
        } else if (number) {
            return "number";
        } else {
            return null;
        }
    }

    public HtmlClassParser(StartElement elt) {
        javax.xml.stream.events.Attribute attrClass = elt.getAttributeByName(Constants.HTML_CLASS);
        
        if (attrClass != null){
            String[] classNames = attrClass.getValue().split("\\s");
            Arrays.asList(classNames).forEach(className -> {
                if (Constants.OBJECT_CLASS_NAME.equals(className)){
                    dataObject = true;
                } else if (className.startsWith(Constants.OBJECTID_CLASS_PREFIX)) {
                    dataObjectId = className.substring(Constants.OBJECTID_CLASS_PREFIX.length());
                } else if (className.startsWith(Constants.OBJECT_TYPE_CLASS_PREFIX)) {
                    dataObjectType = className.substring(Constants.OBJECT_TYPE_CLASS_PREFIX.length());
                } else if (className.startsWith(Constants.ATTRIBUTE_NAME_CLASS_PREFIX)){
                    dataAttributeName = className.substring(Constants.ATTRIBUTE_NAME_CLASS_PREFIX.length());
                } else if (className.startsWith(Constants.ATTRIBUTE_TYPE_CLASS_PREFIX)){
                    dataAttributeType = className.substring(Constants.ATTRIBUTE_TYPE_CLASS_PREFIX.length());
                } else if (className.equals(Constants.VALIDATOR_NUMBER)){
                    number = true;
                } else if (className.equals(Constants.VALIDATOR_CSV)){
                    csv = true;
                }
            });
        }
    }
}
