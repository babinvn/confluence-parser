package me.bvn.confluence.tests;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.Appender;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import me.bvn.confluence.parser.DataParser;
import me.bvn.confluence.parser.DummyPageResolver;
import me.bvn.confluence.parser.DummyUserResolver;
import me.bvn.confluence.parser.model.PageResolver;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import org.junit.Test;

import me.bvn.confluence.parser.SchemaParser;
import me.bvn.confluence.parser.model.UserResolver;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(MockitoJUnitRunner.class)
public class SchemaTest {
    private static final Logger LOGGER = LoggerFactory.getLogger("me.bvn.confluence.parser");
    // Online: http://json-schema-validator.herokuapp.com
    private static final JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

    @Mock
    Appender appender;

    @Before
    public void setup() {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(Logger.ROOT_LOGGER_NAME);
        when(appender.getName()).thenReturn("MOCK");
        when(appender.isStarted()).thenReturn(true);
        logger.addAppender(appender);
        logger.setLevel(Level.ALL);
    }
    
    @Test
    public void testGenerateSchema() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        String serializedSchema = null;
        try (InputStream in = this.getClass().getResourceAsStream("/sample.template.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                ObjectNode root = SchemaParser.parse(reader, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                serializedSchema = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serializedSchema);
            }
        }
        ObjectNode data;
        try (InputStream in = this.getClass().getResourceAsStream("/sample.data.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                data = mapper.readValue(serialized, ObjectNode.class);
            }
        }

        JsonNode schema = JsonLoader.fromString(serializedSchema);
        ProcessingReport report = VALIDATOR.validate(schema, data);
        assert(report.isSuccess());
    }

    @Test
    public void testGenerateObjSchema() throws IOException, XMLStreamException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.template2.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                ObjectNode root = SchemaParser.parse(reader, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
            }
        } catch (Throwable ex) {
            ex.printStackTrace(System.out);
        }
    }

    @Test
    public void testGenerateTableSchema() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        String serializedSchema = null;
        try (InputStream in = this.getClass().getResourceAsStream("/sample.table.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                ObjectNode root = SchemaParser.parse(reader, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                serializedSchema = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serializedSchema);
            }
        }
    }
}
