package me.bvn.confluence.tests;

import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import me.bvn.confluence.parser.DataParser;
import me.bvn.confluence.parser.DummyPageResolver;
import me.bvn.confluence.parser.DummyUserResolver;
import me.bvn.confluence.parser.model.PageResolver;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import org.junit.Test;

import me.bvn.confluence.parser.model.UserResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataTest {
    private static final Logger LOGGER = LoggerFactory.getLogger("me.bvn.confluence.parser");


    // Online: http://json-schema-validator.herokuapp.com
    private static final JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();
    
    @Test
    public void testParseData() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        ObjectNode data;
        try (InputStream in = this.getClass().getResourceAsStream("/sample.data.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                data = mapper.readValue(serialized, ObjectNode.class);
            }
        }
    }
}
