package me.bvn.confluence.tests;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.Appender;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import org.junit.Test;

import me.bvn.confluence.parser.DummyUserResolver;
import me.bvn.confluence.parser.DummyPageResolver;
import me.bvn.confluence.parser.model.PageResolver;
import me.bvn.confluence.parser.DataParser;
import me.bvn.confluence.parser.model.UserResolver;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParserTest {
    private static final Logger LOGGER = LoggerFactory.getLogger("me.bvn.confluence.parser");
    // Online: http://json-schema-validator.herokuapp.com
    private static final JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

    @Mock
    Appender appender;

    @Before
    public void setup() {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(Logger.ROOT_LOGGER_NAME);
        when(appender.getName()).thenReturn("MOCK");
        when(appender.isStarted()).thenReturn(true);
        logger.addAppender(appender);
        logger.setLevel(Level.ALL);
    }

    @Test
    public void testNumber() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.number.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("number1").isNull());
                assert(root.get("number2").isNull());
                assert(root.get("number3").asLong() == 123l);
                assert(root.get("number4").asDouble() == -123.87);
                assert(root.get("number5").isArray());
                assert(root.get("number5").size() == 2);
                assert(root.get("number5").get(0).asDouble() == 789.56);
                assert(root.get("number5").get(1).asInt() == 10);
                assert(root.get("number6").asDouble() == -123.87);
                assert(root.get("number7").size() == 3);
                assert(root.get("number7").get(0).asDouble() == -123.87);
                assert(root.get("number7").get(1).asInt() == 88);
                assert(root.get("number7").get(2).asInt() == 99);
                
                JsonNode schema = JsonLoader.fromResource("/template.number.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
                //report.forEach(pm -> System.out.println(pm.getMessage()) );
            }
        }
    }

    @Test
    public void testBoolean() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.boolean.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("boolean1").isNull());
                assert(root.get("boolean2").isNull());
                assert(root.get("boolean3").asBoolean() == false);
                assert(root.get("boolean4").asBoolean() == true);
                assert(root.get("boolean5").isArray());
                assert(root.get("boolean5").size() == 3);
                assert(root.get("boolean5").get(0).asBoolean() == false);
                assert(root.get("boolean5").get(1).asBoolean() == true);
                assert(root.get("boolean5").get(2).asBoolean() == true);
                assert(root.get("boolean6").size() == 3);
                assert(root.get("boolean6").get(0).asText().equals("☐ Boolean value1 as text"));
                assert(root.get("boolean6").get(1).asText().equals("☑ Boolean value2 as text"));

                JsonNode schema = JsonLoader.fromResource("/template.boolean.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }

    @Test
    public void testDate() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.date.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("date1").isNull());
                assert(root.get("date2").isNull());
                assert(root.get("date3").isObject());
                assert(root.get("date3").get("_type").asText().equals("date"));
                assert(root.get("date3").get("date").asText().equals("2020-03-16"));
                assert(root.get("date4").isArray());
                assert(root.get("date4").size() == 3);
                assert(root.get("date4").get(0).isObject());
                assert(root.get("date5").size() == 4);
                assert(root.get("date5").get(0).asText().equals("2020-03-16"));
                assert(root.get("date5").get(3).asText().equals("123"));

                JsonNode schema = JsonLoader.fromResource("/template.date.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }

    @Test
    public void testUser() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.user.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("user1").isNull());
                assert(root.get("user2").isNull());
                assert(root.get("user3").isObject());
                assert(root.get("user3").get("_type").asText().equals("user"));
                assert(root.get("user3").get("key").asText().equals("4028817e60284fbc0160285276680000"));
                assert(root.get("user4").isArray());
                assert(root.get("user4").size() == 2);
                assert(root.get("user4").get(0).isObject());
                assert(root.get("user5").size() == 2);
                assert(root.get("user5").get(0).asText().equals("4028817e60284fbc0160285276680000"));

                JsonNode schema = JsonLoader.fromResource("/template.user.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }

    @Test
    public void testPage() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.page.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("page1").isNull());
                assert(root.get("page2").isNull());
                assert(root.get("page3").isObject());
                assert(root.get("page3").get("_type").asText().equals("page"));
                assert(root.get("page3").get("title").asText().equals("Описание API"));
                assert(root.get("page4").isArray());
                assert(root.get("page4").size() == 2);
                assert(root.get("page4").get(0).isObject());
                assert(root.get("page5").size() == 2);
                assert(root.get("page5").get(0).asText().equals("Test Parser User"));

                JsonNode schema = JsonLoader.fromResource("/template.page.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }

    @Test
    public void testLink() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.link.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("link1").isNull());
                assert(root.get("link2").isNull());
                assert(root.get("link3").isObject());
                assert(root.get("link3").get("_type").asText().equals("link"));
                assert(root.get("link3").get("href").asText().equals("https://www.rbc.ru"));
                assert(root.get("link4").isArray());
                assert(root.get("link4").size() == 2);
                assert(root.get("link4").get(0).isObject());
                assert(root.get("link5").size() == 2);
                assert(root.get("link5").get(0).asText().equals("Inosmi"));

                JsonNode schema = JsonLoader.fromResource("/template.link.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }

    @Test
    public void testObject() throws IOException, XMLStreamException, ProcessingException {
        XmlEventReaderFactory xmlEventReaderFactory = new DefaultXmlEventReaderFactory();
        try (InputStream in = this.getClass().getResourceAsStream("/sample.object.xml")) {
            try (InputStreamReader sr = new InputStreamReader(in)) {
                XMLEventReader reader = xmlEventReaderFactory.createStorageXmlEventReader(sr);
                UserResolver userResolver = new DummyUserResolver();
                PageResolver pageResolver = new DummyPageResolver();
                ObjectNode root = DataParser.parse(reader, userResolver, pageResolver, LOGGER);
                ObjectMapper mapper = new ObjectMapper();
                String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
                System.out.println(serialized);
                root = mapper.readValue(serialized, ObjectNode.class);

                assert(root.get("projects").isArray());
                assert(root.get("projects").size() == 2);
                assert(root.get("projects").get(0).isObject());
                assert(root.get("projects").get(1).isObject());
                
                JsonNode schema = JsonLoader.fromResource("/template.object.json");
                ProcessingReport report = VALIDATOR.validate(schema, root);
                assert(report.isSuccess());
            }
        }
    }
}
